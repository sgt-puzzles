Source: sgt-puzzles
Maintainer: Ben Hutchings <ben@decadent.org.uk>
Section: games
Priority: optional
Build-Depends: debhelper (>= 9), halibut, libgtk-3-dev, liblocale-po-perl, perl, po4a, autoconf, automake
Standards-Version: 4.0.1
Homepage: https://www.chiark.greenend.org.uk/~sgtatham/puzzles/
Vcs-Git: https://git.decadent.org.uk/git/sgt-puzzles.git/
Vcs-Browser: https://git.decadent.org.uk/gitweb?p=sgt-puzzles.git

Package: sgt-puzzles
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: yelp | khelpcenter | www-browser
Description: Simon Tatham's Portable Puzzle Collection - 1-player puzzle games
 Simon Tatham's Portable Puzzle Collection contains a number of popular
 puzzle games for one player.  It currently consists of these games:
 .
  * Black Box, ball-finding puzzle
  * Bridges, bridge-placing puzzle
  * Cube, rolling cube puzzle
  * Dominosa, domino tiling puzzle
  * Fifteen, sliding block puzzle
  * Filling, polyomino puzzle
  * Flip, tile inversion puzzle
  * Galaxies, symmetric polyomino puzzle
  * Guess, combination-guessing puzzle
  * Inertia, gem-collecting puzzle
  * Keen, arithmetic Latin square puzzle
  * Light Up, light-bulb placing puzzle
  * Loopy, loop-drawing puzzle
  * Magnets, magnet-placing puzzle
  * Map, map-colouring puzzle
  * Mines, mine-finding puzzle
  * Net, network jigsaw puzzle
  * Netslide, toroidal sliding network puzzle
  * Pattern
  * Pearl, loop-drawing puzzle
  * Pegs, peg solitaire puzzle
  * Range, visible-distance puzzle
  * Rectangles
  * Same Game, block-clearing puzzle
  * Signpost, square-connecting puzzle
  * Singles, number-removing puzzle
  * Sixteen, toroidal sliding block puzzle
  * Slant, maze-drawing puzzle
  * Solo, number placement puzzle
  * Tents, tent-placing puzzle
  * Towers, tower-placing Latin square puzzle
  * Twiddle, rotational sliding block puzzle
  * Undead, monster-placing puzzle
  * Unequal, Latin square puzzle
  * Unruly, black and white grid puzzle
  * Untangle, planar graph layout puzzle
